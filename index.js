/**
 * @format
 */

import { AppRegistry, YellowBox } from "react-native";
import App from "./src/containers/Root";
import { name as appName } from "./app.json";
import TrackPlayer from "react-native-track-player";

YellowBox.ignoreWarnings(["Require cycle:", '"Warning: Provider"']);

AppRegistry.registerComponent(appName, () => App);

TrackPlayer.registerPlaybackService(() =>
  require("./src/services/playerService")
);

TrackPlayer.updateOptions({
  // Whether the player should stop running when the app is closed on Android
  stopWithApp: true,

  // An array of media controls capabilities
  // Can contain CAPABILITY_PLAY, CAPABILITY_PAUSE, CAPABILITY_STOP, CAPABILITY_SEEK_TO,
  // CAPABILITY_SKIP_TO_NEXT, CAPABILITY_SKIP_TO_PREVIOUS, CAPABILITY_SET_RATING
  capabilities: [
    TrackPlayer.CAPABILITY_PLAY,
    TrackPlayer.CAPABILITY_PAUSE,
    TrackPlayer.CAPABILITY_STOP
  ],

  // An array of capabilities that will show up when the notification is in the compact form on Android
  compactCapabilities: [
    TrackPlayer.CAPABILITY_PLAY,
    TrackPlayer.CAPABILITY_PAUSE
  ]
});
