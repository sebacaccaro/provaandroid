export const markers = [
  {
    name: "Ufficio",
    coords: {
      latitude: 45.4092947,
      longitude: 11.9104901
    },
    notififyDistance: 10,
    image: require(`../../assets/assets/Location/id/tag.jpg`)
  },
  {
    name: "Fermata Bus",
    coords: {
      latitude: 45.409567,
      longitude: 11.909445
    },
    notififyDistance: 10,
    image: require(`../../assets/assets/Location/id/bus.jpg`)
  },
  {
    name: "Caffe Carlotta",
    coords: {
      latitude: 45.40977,
      longitude: 11.910558
    },
    notififyDistance: 10,
    image: require(`../../assets/assets/Location/id/caffe-carlotta.jpg`)
  },
  {
    name: "Vinile",
    coords: {
      latitude: 45.73012,
      longitude: 11.761574
    },
    notififyDistance: 10,
    image: require(`../../assets/assets/Location/id/vinile.png`)
  }
];
