/* @flow @format */
export default {
  "35132:1200": {
    minor: 1200,
    uniqueId: "EXVx",
    txPower: 3,
    mac: "F7:27:49:8B:2D:F9",
    major: 35132,
    proximity: "f7826da6-4fa2-4e98-8024-bc5b71e0893e",
    name: "MuVe200",
    interval: 625
  },
  "35132:1198": {
    minor: 1198,
    uniqueId: "nM7b",
    txPower: 3,
    mac: "E9:32:DF:7D:B0:3B",
    major: 35132,
    proximity: "f7826da6-4fa2-4e98-8024-bc5b71e0893e",
    name: "MuVe198",
    interval: 625
  },
  "35132:1199": {
    minor: 1199,
    uniqueId: "5Wkz",
    txPower: 3,
    mac: "C1:A6:CB:EA:A3:4F",
    major: 35132,
    proximity: "f7826da6-4fa2-4e98-8024-bc5b71e0893e",
    name: "MuVe199",
    interval: 625
  }
};
