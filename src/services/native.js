/* @flow */

import AsyncStorage from "@react-native-community/async-storage";
import type { Post } from "../types/Post";

const bookmarksKey = "@MiaApp:bookmarks";

const setBookmarks = async (bookmarks: Array<Post>) => {
  console.log(bookmarks);
  try {
    await AsyncStorage.setItem(bookmarksKey, JSON.stringify(bookmarks || []));
  } catch (error) {
    console.log(`setBookmarks error `, error);
    // Error saving data
  }
};

const getBookmarks = async () => {
  let bookmarks = [];
  try {
    const value = await AsyncStorage.getItem(bookmarksKey);
    if (value !== null) {
      bookmarks = JSON.parse(value);
    } else {
      console.log(`Bookmarks error: `, value);
    }
  } catch (error) {
    // Error retrieving data
  }
  return bookmarks;
};

export default {
  setBookmarks,
  getBookmarks
};
