/* @flow @format */
import type { Post } from "../types/Post";

export const fetchSubSet = async (
  subSet: Array<number>
): Promise<Array<Post>> => {
  const subsSetPromise = subSet.map(news => fetchOne(news));
  const posts = await Promise.all(subsSetPromise);
  return posts;
};

export const fetchNews = async (): Promise<Array<number>> => {
  const response = await fetch(
    "https://hacker-news.firebaseio.com/v0/newstories.json"
  );
  const result: Promise<Array<number>> = await response.json();
  return result;
};

export const fetchOne = async (newsId: number): Promise<Object> => {
  const result = await fetch(
    `https://hacker-news.firebaseio.com/v0/item/${newsId}.json`
  );
  const resultJSON = await result.json();
  return resultJSON;
};
