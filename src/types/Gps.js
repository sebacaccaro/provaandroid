export type Coord = {
  latitude: number,
  longitude: number
};

export type GpsMarker = {
  name: string,
  coords: Coord,
  notifyDistance: Number, //measured in meters
  image: string
};

export type GpsCardInfo = {
  name: string,
  distance: number,
  notifyDistance: Number, //measured in meters
  image: string
};
