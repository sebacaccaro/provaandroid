/** @flow @format */
import AuthStore from "../stores/auth";
import PostStore from "../stores/post";

export type Stores = {|
  postStore: PostStore,
  authStore: AuthStore
|};
