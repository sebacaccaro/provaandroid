/** @flow @format */
import type { Post } from "./Post";

export type News = {
  index: number,
  item: Post,
  separators: Array<Object>
};
