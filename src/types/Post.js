/* @flow @format */

export type Post = {
  bookmarked?: boolean,
  by?: string,
  descendants?: number,
  fetch?: number => void,
  fetchId: number,
  id?: number,
  loaded?: boolean,
  score?: number,
  time?: number,
  title?: string,
  url?: string,
  toggleBookmark?: number => void
};
