/* @flow @format */
import React, { Component } from "react";
import { Slider, View, Text, StyleSheet } from "react-native";
import TrackPlayer, { ProgressComponent } from "react-native-track-player";

type Props = {};
type State = {};

export default class TrackStatus extends ProgressComponent<Props, State> {
  render() {
    return (
      <Slider
        style={this.props.style}
        value={this.state.position}
        maximumValue={this.state.duration}
        onValueChange={value => TrackPlayer.seekTo(value)}
      />
    );
  }
}

const styles = StyleSheet.create({});
