/* @flow @format */
import React, { Component } from "react";
import {
  Slider,
  View,
  Text,
  StyleSheet,
  TouchableHighlight
} from "react-native";
import Tts from "react-native-tts";
import { Button } from "react-native-paper";
import { observable, computed } from "mobx";
import { observer } from "mobx-react/native";
import { IconButton } from "react-native-paper";
import Icon from "react-native-ionicons";

type Props = {};
type State = {};

@observer
export default class TtsHandler extends Component<Props, State> {
  componentDidMount() {
    Tts.addEventListener("tts-finish", event => this.nextPhrase());
  }

  @observable currentPhrase: number = 0;

  @computed
  get phrases() {
    return this.props.text.split(/\.|;|!|\?|!/).filter(phrase => phrase !== "");
  }

  isThereNextPhrase = (): boolean => {
    console.log(this.currentPhrase, this.phrases, this.phrases.length);
    return this.currentPhrase < this.phrases.length - 1;
  };

  isTherePreviusPhrase = (): boolean => {
    return this.currentPhrase > 0;
  };

  nextPhrase = (): void => {
    Tts.stop();
    if (this.isThereNextPhrase()) {
      this.currentPhrase++;
      this.playCurrent();
    }
  };

  previusPhrase = () => {
    Tts.stop();
    if (this.isTherePreviusPhrase()) {
      this.currentPhrase--;
      this.playCurrent();
    }
  };

  playCurrent = () => {
    Tts.getInitStatus().then(() => {
      Tts.speak(this.phrases[this.currentPhrase]);
    });
  };

  skipToPhrase = (phraseNumber: number) => {
    Tts.stop();
    this.currentPhrase = phraseNumber;
    this.playCurrent();
  };

  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.props.text !== prevProps.text) this.currentPhrase = 0;
  }

  render() {
    Tts.voices().then(voices =>
      console.log(voices.filter(l => l.language === "it-IT"))
    );
    console.log(this.phrases);
    return (
      <View style={styles.container}>
        <Text>{`Phrase ${this.currentPhrase + 1} of ${
          this.phrases.length
        }`}</Text>
        <View style={styles.controls}>
          <IconButton
            icon="skip-previous"
            size={26}
            style={{ marginBottom: -3 }}
            onPress={() => this.previusPhrase()}
          />
          <IconButton
            icon="play-arrow"
            size={26}
            onPress={() => this.playCurrent()}
            style={{ marginBottom: -3 }}
          />
          <IconButton
            icon="stop"
            size={26}
            style={{ marginBottom: -3 }}
            onPress={() => Tts.stop()}
          />
          <IconButton
            icon="skip-next"
            size={26}
            style={{ marginuBottom: -3 }}
            onPress={() => this.nextPhrase()}
          />
        </View>
        <Slider
          style={styles.progessBar}
          value={this.currentPhrase}
          maximumValue={this.phrases.length - 1}
          step={1}
          onValueChange={phraseNumber => this.skipToPhrase(phraseNumber)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  progessBar: {
    width: "80%"
  },
  controls: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    width: "100%"
  }
});
