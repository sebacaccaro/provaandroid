/* @flow @format */

import React from "react";
import {
  Image,
  Animated,
  Easing,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableWithoutFeedback,
  View
} from "react-native";
import { NavigationScreenProps } from "react-navigation";
import Icon from "react-native-ionicons";
import { withNavigation } from "react-navigation";
import DeviceInfo from "react-native-device-info";
import { observable } from "mobx";
import { observer, inject } from "mobx-react/native";
import type { News } from "../types/News";

type Props = { news: News, navigation: NavigationScreenProps };
type State = {};

@observer
class NewsCard extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.fadeIn = new Animated.Value(0);
  }

  @observable fadeIn = null;
  @observable heartScale = new Animated.Value(1);

  componentDidMount() {
    Animated.timing(this.fadeIn, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true
    }).start();
  }

  goToLink(url, title) {
    this.props.navigation.navigate("Post", {
      url,
      title
    });
  }

  favouriteIcon = bookmarked => {
    return bookmarked ? "md-heart" : "md-heart-empty";
  };

  color = bookmarked => {
    return bookmarked ? "red" : "white";
  };

  bounceHeart = () => {
    Animated.sequence([
      Animated.timing(this.heartScale, {
        toValue: 1.6,
        duration: 100,
        easing: Easing.linear,
        useNativeDriver: true
      }),
      Animated.timing(this.heartScale, {
        toValue: 1,
        duration: 600,
        easing: Easing.bounce,
        useNativeDriver: true
      })
    ]).start();
  };

  render() {
    const { news } = this.props;
    const { toggleBookmark, id, url } = news.item;
    if (id && url && toggleBookmark != null)
      return (
        <View style={styles.card}>
          <Animated.View
            style={{
              ...styles.heartView,
              transform: [{ scale: this.heartScale }]
            }}
          >
            <TouchableWithoutFeedback
              style={styles.clickable}
              onPress={() => {
                if (!news.item.bookmarked) {
                  this.bounceHeart();
                }
                toggleBookmark(id);
              }}
            >
              <Icon
                style={styles.heartIcon}
                name={this.favouriteIcon(news.item.bookmarked)}
                size={40}
                color={this.color(news.item.bookmarked)}
              />
            </TouchableWithoutFeedback>
          </Animated.View>
          <TouchableHighlight
            key={`${id}card`}
            onPress={() => this.goToLink(url, news.item.title)}
          >
            <View style={styles.row} onPress={this.goToLink}>
              <Image
                style={styles.cardImg}
                source={{
                  uri:
                    "https://picsum.photos/500/300?rand=" + new Date().getTime()
                }}
              />
              <Text numberOfLines={2} style={styles.title}>
                {news.item.title}
              </Text>
              <Text style={styles.author}>{news.item.by}</Text>
            </View>
          </TouchableHighlight>
        </View>
      );
    else return null;
  }
}

export default withNavigation(NewsCard);

const cardRadius = 5;
const styles = StyleSheet.create({
  card: {
    width: "50%" //
  },
  row: {
    paddingBottom: 15,
    margin: 2,
    backgroundColor: "white",
    borderRadius: cardRadius,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5
  },
  cardImg: {
    alignSelf: "stretch",
    height: 150,
    borderTopLeftRadius: cardRadius,
    borderTopRightRadius: cardRadius
  },
  title: {
    fontSize: 20,
    margin: 5,
    marginBottom: 2,
    fontWeight: "bold",
    height: 50 //
  },
  author: {
    fontSize: 15,
    margin: 5,
    marginTop: 0,
    color: "gray"
  },
  heartIcon: {
    zIndex: 9
  },
  heartView: {
    zIndex: 2,
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    top: 0,
    right: 0,
    width: 60,
    height: 60
  },
  clickable: {
    width: "100%",
    height: "100%"
  }
});

if (!DeviceInfo.isTablet()) {
  styles.card = null;
  styles.title.height = null;
}
