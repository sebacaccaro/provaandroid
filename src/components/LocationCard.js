import React, { Component } from "react";
import {
  Text,
  View,
  TouchableHighlight,
  StyleSheet,
  ImageBackground
} from "react-native";
import Device from "react-native-device-detection";
import LinearGradient from "react-native-linear-gradient";
import type { Coord, GpsCardInfo } from "../types/Gps";
import { withNavigation } from "react-navigation";
import DeviceInfo from "react-native-device-info";
import w from "../constants/Layout";

type State = {};
type Props = { marker: GpsCardInfo };

class LocationCard extends Component {
  formattedDistance = distanceinMeters => {
    if (distanceinMeters >= 1000)
      return (distanceinMeters / 1000).toFixed(1) + "km";
    else return distanceinMeters.toString() + "m";
  };

  goToPlace(placeName, title) {
    this.props.navigation.navigate("Place", {
      placeName,
      title
    });
  }

  render() {
    const { marker } = this.props;
    const { name, distance, image } = marker.item;
    return (
      <View style={styles.card}>
        <TouchableHighlight onPress={() => this.goToPlace(name)}>
          <View style={styles.row} onPress={this.goToLink}>
            <ImageBackground style={styles.cardImg} source={image}>
              <LinearGradient
                colors={["transparent", "black"]}
                style={styles.cardOverlay}
              />
            </ImageBackground>
            <Text numberOfLines={2} style={styles.title}>
              {`${name}`}
            </Text>
            <Text style={styles.heartView}>
              {this.formattedDistance(distance)}
            </Text>
          </View>
        </TouchableHighlight>
      </View>
    );
  }
}

export default withNavigation(LocationCard);

const cardRadius = 5;

const styles = StyleSheet.create({
  card: {
    width: w.window.width / (1 + DeviceInfo.isTablet())
  },
  row: {
    margin: 2,
    backgroundColor: "white",
    borderRadius: cardRadius,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5
  },
  cardImg: {
    alignSelf: "stretch",
    width: "100%",
    height: 150,
    borderRadius: cardRadius,
    zIndex: 0
  },
  title: {
    fontSize: 25,
    fontWeight: "bold",
    zIndex: 20,
    color: "white",
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    bottom: 35,
    left: 20
  },
  heartView: {
    zIndex: 20,
    position: "absolute",
    bottom: 13,
    left: 22,
    height: 25,
    color: "white",
    fontSize: 20
  },
  clickable: {
    width: "100%",
    height: "100%"
  },
  cardOverlay: {
    borderRadius: cardRadius,
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  }
});

/* if (!DeviceInfo.isTablet()) {
  styles.card.width = null;
} */
