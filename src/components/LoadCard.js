/** @flow @format */
import Placeholder, { Line, ImageContent, Media } from "rn-placeholder";
import { StyleSheet, FlatList, View, Text } from "react-native";
import React, { Component } from "react";
import NewsCard from "./NewsCard";
import type { News } from "../types/News";

const ComponentLoaded = () => <Text>I'm loaded!</Text>;

/* type OwnProps = {| navigation: NavigationScreenProps |};
type State = {};
type Props = StoreProps & OwnProps; */

type Props = { news: News };
type State = {};

class LoadCard extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    const { fetchId, fetch } = props.news.item;
    if (fetch != null) fetch(fetchId);
  }

  shouldComponentUpdate(nextProps: Props) {
    const { title, bookmarked } = this.props.news.item;
    return (
      title !== nextProps.news.item.title ||
      bookmarked !== nextProps.news.item.bookmarked
    );
  }

  render() {
    const { news } = this.props;
    const { title } = news.item;
    if (!title)
      return (
        <Placeholder isReady={false} style={styles.card}>
          <Line width="100%" textSize={150} style={styles.line} />
          <Line width="95%" textSize={20} style={styles.line} />
          <Line width="80%" textSize={20} style={styles.line} />
          <Line
            width="30%"
            textSize={15}
            style={{ ...styles.line, marginBottom: 10 }}
          />
        </Placeholder>
      );
    else return <NewsCard news={news} />;
  }
}

const styles = StyleSheet.create({
  line: { marginBottom: 5, borderRadius: 0 }, //, marginLeft: 15, marginRight: 10 },
  card: {
    paddingBottom: 15,
    margin: 2,
    backgroundColor: "white",
    borderRadius: 5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5
  }
});

export default LoadCard;
