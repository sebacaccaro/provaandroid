/* @flow @format */
import React, { Component } from "react";
import Icon from "react-native-ionicons";

import Colors from "../constants/Colors";

type Props = { name: string, focused: Boolean };
type State = {};

export default class TabBarIcon extends Component<Props, State> {
  render() {
    return (
      <Icon
        name={this.props.name}
        size={26}
        style={{ marginBottom: -3 }}
        color={
          this.props.focused ? Colors.tabIconSelected : Colors.tabIconDefault
        }
      />
    );
  }
}
