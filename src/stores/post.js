/* @flow @format */
import { observable, action, computed, autorun } from "mobx";
import { fetchSubSet, fetchNews, fetchOne } from "../services/newsService";
import postServices from "../services/native";
import type { Post } from "../types/Post";

export default class PostStore {
  @observable postIds: Array<number> = [];
  @observable posts: Array<Post> = [];
  @observable bookmarks: Array<Post> = [];

  _disposePersister = null;
  lastCall: number = 0;

  @action
  fetchPostsIds = async (): Promise<void> => {
    this.postIds = await fetchNews();
  };

  @action
  fetchPostsSubSet = async (subSet: Array<number>): Promise<void> => {
    const toFetch = subSet.filter(
      post => this.posts.map(offlinePost => offlinePost.id).indexOf(post) < 0
    );
    const newPost = await fetchSubSet(toFetch);
    this.posts = [...this.posts, ...newPost];
  };

  @action fetchSomePosts = async (numberOfPosts: number): Promise<void> => {
    const millisecondsStamp = Math.floor(Date.now());
    if (millisecondsStamp - this.lastCall > 3000) {
      let toFetch = this.postIds.splice(0, 10).map(post => ({
        fetchId: post
      }));
      this.posts = [...this.posts, ...toFetch];
      //await this.fetchPostsSubSet(toFetch);
    }
  };

  @action
  initNews = async (): Promise<void> => {
    this.bookmarks = await postServices.getBookmarks();
    this._disposePersister = autorun(this._persist);
    await this.fetchPostsIds();
    let toFetch = this.postIds.splice(0, 10).map(post => ({
      fetchId: post
    }));
    this.posts = [...this.posts, ...toFetch];
  };

  @action
  toggleBookmark = (postId: number) => {
    let bookmarks = Array.from(this.bookmarks);

    if (this.postInBookmarks(postId))
      bookmarks = bookmarks.filter(bookmarkPost => bookmarkPost.id !== postId);
    else {
      const postToBookmark: void | Post = this.posts.find(
        post => post.id === postId
      );
      if (postToBookmark) bookmarks.push(postToBookmark);
    }
    this.bookmarks = bookmarks;
  };

  postInBookmarks = (postId: number): ?Post => {
    return this.bookmarks.find(bookmarkPost => bookmarkPost.id === postId);
  };

  @action
  fetchPost = async (postId: number) => {
    let fetchedPost;
    const postIfExist = this.postInBookmarks(postId);
    if (!postIfExist) fetchedPost = await fetchOne(postId);
    else fetchedPost = postIfExist;
    const index = this.posts.map(post => post.fetchId).indexOf(postId);
    this.posts[index] = { ...fetchedPost, fetchId: fetchedPost.id };
  };

  @computed
  get homePosts(): Array<Post> {
    //$FlowFixMe
    const postsWithBookmarkInfo: Array<Post> = this.posts
      .filter(post => post !== null)
      .map(post => {
        let loaded = this.posts.map(post => post.id).indexOf(post.fetchId) >= 0;
        let bookmarked = post.id
          ? this.bookmarks.map(post => post.id).indexOf(post.id) >= 0
          : false;
        return {
          ...post,
          bookmarked,
          toggleBookmark: this.toggleBookmark,
          fetch: this.fetchPost,
          loaded
        };
      });
    return postsWithBookmarkInfo;
  }

  @computed
  get bookmarkedPosts(): Array<Post> {
    //$FlowFixMe
    return this.bookmarks.map(post => ({
      ...post,
      bookmarked: true,
      toggleBookmark: this.toggleBookmark,
      fetch: this.fetchPost,
      loaded: true
    }));
  }

  _persist = () => {
    postServices.setBookmarks(this.bookmarks);
  };
}
