/* @flow @format */
import { observable, action, computed, autorun } from "mobx";

export default class AuthStore {
  @observable username = null;

  @action
  login = (username: string, password: string) => {
    this.username = username;
  };
}
