/* @flow @format */
import React, { Component } from "react";
import { Provider, observer } from "mobx-react/native";
import { NavigationScreenProps } from "react-navigation";
import App from "./App";
import PostStore from "../stores/post";
import AuthStore from "../stores/auth";

const postStore = new PostStore();
const authStore = new AuthStore();

type OwnProps = {| navigation: NavigationScreenProps |};
type State = {};
type Props = OwnProps;

@observer
export default class Root extends Component<Props, State> {
  render() {
    return (
      <Provider postStore={postStore} authStore={authStore}>
        {/* $FlowFixMe */}
        <App />
      </Provider>
    );
  }
}
