/* @flow @format */
import React from "react";
import { WebView, StyleSheet } from "react-native";
import { NavigationScreenProps } from "react-navigation";

type OwnProps = {| navigation: NavigationScreenProps |};
type State = {};
type Props = OwnProps;

export default class PostScreen extends React.Component<Props, State> {
  static navigationOptions = ({
    navigation
  }: {
    navigation: NavigationScreenProps
  }) => {
    return {
      title: navigation.getParam("title", "News")
    };
  };

  render() {
    return (
      <WebView
        style={styles.webview}
        originWhitelist={["*"]}
        source={{
          uri: this.props.navigation.getParam("url", "http://google.com")
        }}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: "#fff"
  }
});
