/* @flow @format */
import React, { Component } from "react";
import { StyleSheet, FlatList, View } from "react-native";
import { observer, inject } from "mobx-react/native";
import NewsCard from "../components/NewsCard";
import { observable } from "mobx";
import { NavigationScreenProps } from "react-navigation";
import Device from "react-native-device-detection";
import type { Post } from "../types/Post";
import type { Stores } from "../types/Stores";
import type { News } from "../types/News";

type StoreProps = {|
  initNews: () => void,
  bookmarkedPosts: Array<Post>
|};

type OwnProps = {| navigation: NavigationScreenProps |};
type State = {};
type Props = StoreProps & OwnProps;

const mapStoresToProps = (stores: Stores, props: Props) => {
  return {
    initNews: stores.postStore.initNews,
    bookmarkedPosts: stores.postStore.bookmarkedPosts
  };
};

@inject(mapStoresToProps)
@observer
export default class Bookmarks extends Component<Props, State> {
  renderNews = (news: News) => {
    return <NewsCard news={news} />;
  };

  extractKey = (item: Post) => {
    if (item.id) return item.id.toString() + "book";
  };

  static navigationOptions = {
    title: "Bookmarks"
  };

  render() {
    const { bookmarkedPosts } = this.props;

    return (
      <View style={styles.container}>
        <FlatList
          style={styles.container}
          data={bookmarkedPosts}
          renderItem={this.renderNews}
          keyExtractor={this.extractKey}
          numColumns={1 + Device.isTablet}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  }
});
