/* @flow @format */
import React, { Component } from "react";
import { observer } from "mobx-react/native";
import { observable } from "mobx";
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  View
} from "react-native";
import QRCodeScanner from "react-native-qrcode-scanner";
import { NavigationScreenProps } from "react-navigation";

type StoreProps = {};
type OwnProps = {| navigation: NavigationScreenProps |};
type State = {};
type Props = StoreProps & OwnProps;

@observer
export default class ScanScreen extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { focusedScreen: false };
  }

  @observable focusedScreen = false;
  @observable error = "";

  componentDidMount() {
    const { navigation } = this.props;
    navigation.addListener("willFocus", () => {
      this.focusedScreen = true;
    });
    navigation.addListener("willBlur", () => {
      this.focusedScreen = false;
    });
  }

  goToLink(url: string, title?: string) {
    this.props.navigation.navigate("Page", {
      url,
      title
    });
  }

  onSuccess = (e: { data: string }) => {
    console.log(e);
    this.goToLink(e.data);
  };

  render() {
    if (this.focusedScreen) {
      return this.cameraView();
    } else {
      return <View />;
    }
  }

  static navigationOptions = {
    title: "QR Reader"
  };

  cameraView = () => {
    return (
      <QRCodeScanner
        style={{ flex: 1 }}
        showMarker
        onRead={this.onSuccess}
        cameraStyle={styles.camera}
        containerStyle={styles.cameraCont}
        topContent={
          <Text style={styles.centerText}>{"Scan code to open webPage"}</Text>
        }
        bottomContent={
          <TouchableOpacity style={styles.buttonTouchable}>
            <Text style={styles.buttonText}>{this.error}</Text>
          </TouchableOpacity>
        }
      />
    );
  };
}

const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: "#777"
  },
  textBold: {
    fontWeight: "500",
    color: "#000"
  },
  buttonText: {
    fontSize: 21,
    color: "rgb(0,122,255)"
  },
  buttonTouchable: {
    padding: 16
  }
});

AppRegistry.registerComponent("default", () => ScanScreen);
