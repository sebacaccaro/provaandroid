/* @flow @format */
import React, { Component } from "react";
import { StyleSheet, FlatList, View } from "react-native";
import { observer, inject } from "mobx-react/native";
import NewsCard from "../components/NewsCard";
import { NavigationScreenProps } from "react-navigation";
import LoadCard from "../components/LoadCard";
import { observable } from "mobx";
import DeviceInfo from "react-native-device-info";
import type { Post } from "../types/Post";
import type { Stores } from "../types/Stores";
import type { News } from "../types/News";
import type { Element } from "react";

type StoreProps = {|
  initNews: () => void,
  homePosts: Array<Post>,
  fetchSomePosts: number => void
|};

type OwnProps = {| navigation: NavigationScreenProps |};
type State = {};
type Props = StoreProps & OwnProps;

const mapStoresToProps = (stores: Stores, props: Props) => {
  return {
    initNews: stores.postStore.initNews,
    homePosts: stores.postStore.homePosts,
    fetchSomePosts: stores.postStore.fetchSomePosts
  };
};

@inject(mapStoresToProps)
@observer
export default class Home extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
  }

  componentDidMount() {
    this.props.initNews();
  }

  renderNews = (news: News): Element<typeof LoadCard> => {
    return <LoadCard news={news} />;
  };

  extractKey = (item: Post): string => {
    return `${item.fetchId}`;
  };

  static navigationOptions = {
    title: "ShitNews"
  };

  isCloseToBottom({
    layoutMeasurement,
    contentOffset,
    contentSize
  }: {
    layoutMeasurement: Object,
    contentOffset: Object,
    contentSize: Object
  }) {
    return (
      layoutMeasurement.height + contentOffset.y >= contentSize.height - 50
    );
  }

  render() {
    const { homePosts, fetchSomePosts } = this.props;
    return (
      <View style={styles.container}>
        <FlatList
          style={styles.card}
          data={homePosts}
          renderItem={this.renderNews}
          keyExtractor={this.extractKey}
          numColumns={1 + DeviceInfo.isTablet()}
          onEndReached={() => fetchSomePosts(10)}
          onEndReachedThreshold={0.5}
          //refreshing={this.state.refreshing}
          //onRefresh={() => console.log("los sds  sadsadl")}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "flex-start"
  }
});
