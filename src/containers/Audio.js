/* @flow @format */
import React, { Component } from "react";
import { observer } from "mobx-react/native";
import { observable } from "mobx";
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  View
} from "react-native";
import { NavigationScreenProps } from "react-navigation";
import { PermissionsAndroid } from "react-native";
import TrackPlayer from "react-native-track-player";
import { Button } from "react-native-paper";
import { bigIntLiteral } from "@babel/types";
import TrackStatus from "../components/TrackStatus";
import TtsHandler from "../components/TtsHandler";

type StoreProps = {};
type OwnProps = {| navigation: NavigationScreenProps |};
type State = {};
type Props = StoreProps & OwnProps;

@observer
export default class Audio extends Component<Props, State> {
  setUp = async () => {
    await TrackPlayer.setupPlayer();
    await TrackPlayer.add({
      id: "trackId",
      url: require("./../../assets/assets/audio/Psycho.mp3"),
      title: "Psycho",
      artist: "Muse",
      artwork: require("./../../assets/assets/images/drones.jpg")
    });

    // Starts playing it
    TrackPlayer.play();
  };

  playPause = async () => {
    const lol = await TrackPlayer.getState();
    switch (lol) {
      case TrackPlayer.STATE_NONE:
        this.setUp();
        break;
      case TrackPlayer.STATE_PLAYING:
        TrackPlayer.pause();
        break;
      case TrackPlayer.STATE_PAUSED:
        TrackPlayer.play();
        break;
      case TrackPlayer.STATE_STOPPED:
        this.setUp();
        break;
      default:
        console.log(lol);
        break;
    }
  };

  render() {
    console.log(TrackPlayer);
    return (
      <View style={styles.container}>
        <Text>{"AUDIO PLACEHOLDER"}</Text>
        <Button onPress={() => this.playPause()}>{"Play"}</Button>
        <Button onPress={() => TrackPlayer.stop()}>{"Stop"}</Button>
        <TrackStatus style={styles.player} />
        <TtsHandler
          text={
            "Opera iconica ed enigmatica della pittura mondiale, si tratta sicuramente del ritratto più celebre della storia nonché di una delle opere d'arte più note in assoluto. Il sorriso impercettibile del soggetto, col suo alone di mistero, ha ispirato tantissime pagine di critica, letteratura, opere di immaginazione e persino studi psicoanalitici; sfuggente, ironica e sensuale, la Monna Lisa è stata di volta in volta amata e idolatrata ma anche derisa o aggredita. La Gioconda rappresenta una meta obbligata per migliaia di persone al giorno, tanto che nella grande sala in cui è esposta un cordone deve tenere a debita distanza i visitatori; nella lunga storia del dipinto non sono infatti mancati i tentativi di vandalismo, nonché un furto rocambolesco che in un certo senso ne ha alimentato la leggenda."
          }
        />
      </View>
    );
  }

  static navigationOptions = {
    title: "Audio"
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  textBold: {
    fontWeight: "500",
    color: "#000"
  },
  buttonText: {
    fontSize: 21,
    color: "rgb(0,122,255)"
  },
  buttonTouchable: {
    padding: 16
  },
  title: {
    fontSize: 25
  },
  measure: {
    fontSize: 15
  },
  coord: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  player: {
    width: "80%"
  }
});
