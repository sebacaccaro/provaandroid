/** @flow @format */
import React, { Component } from "react";
import Navigator from "./Navigator";
import AuthNavigator from "./AuthNavigator";
import type { Stores } from "../types/Stores";

import { observer, inject } from "mobx-react/native";

type StoreProps = {|
  username: string
|};

type OwnProps = {};
type State = {};
type Props = StoreProps & OwnProps;

const mapStoresToProps = (stores: Stores, props: Props) => {
  return {
    username: stores.authStore.username
  };
};

@inject(mapStoresToProps)
@observer
export default class App extends Component<Props, State> {
  render() {
    const { username } = this.props;

    if (/* !username */ false) return <AuthNavigator />;
    else return <Navigator />;
  }
}
