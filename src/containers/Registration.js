/* @flow @format */
import { NavigationScreenProps } from "react-navigation";
import React, { Component } from "react";
import {
  StyleSheet,
  FlatList,
  KeyboardAvoidingView,
  View,
  Image,
  Text,
  Animated,
  Keyboard
} from "react-native";
import { observer, inject } from "mobx-react/native";
import { observable } from "mobx";
import { TextInput, HelperText } from "react-native-paper";

type StoreProps = {};
type OwnProps = {| navigation: NavigationScreenProps |};
type State = {};
type Props = StoreProps & OwnProps;

const mapStoresToProps = (stores, props) => {
  return {};
};

@inject(mapStoresToProps)
@observer
export default class Registration extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.fake = new Animated.Value(0);
    this.fadeAnimation = new Animated.Value(0);
    this.fadeAnimation2 = new Animated.Value(0);
    this.boxes = new Animated.Value(0);
  }

  @observable fake = null;
  @observable fadeAnimation = null;
  @observable fadeAnimation2 = null;
  @observable boxes = null;

  componentDidMount() {
    const animations = [
      Animated.timing(this.fadeAnimation, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true
      }),
      Animated.timing(this.fadeAnimation2, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true
      }),
      Animated.timing(this.boxes, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true
      })
    ];
    Animated.stagger(500, animations).start();
  }

  @observable user = "";
  @observable password = "";
  @observable repeatPassword = "";
  @observable isRegistered = false;

  registration = () => {
    const animations = [
      Animated.timing(this.fadeAnimation, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: true
      }),
      Animated.timing(this.fadeAnimation2, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: true
      }),
      Animated.timing(this.boxes, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: true
      })
    ];
    Animated.parallel(animations).start(() => {
      Keyboard.dismiss();
      this.isRegistered = true;
      this.showSucces();
    });
  };

  showSucces = () => {
    const animations = [
      Animated.timing(this.fadeAnimation, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true
      }),
      Animated.timing(this.fadeAnimation2, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true
      })
    ];
    Animated.stagger(500, animations).start();
  };

  passwordsMatching = () =>
    this.password !== this.repeatPassword && this.repeatPassword !== "";

  static navigationOptions = {
    title: "Register"
  };

  render() {
    if (!this.isRegistered)
      return (
        <KeyboardAvoidingView
          style={styles.container}
          behavior="padding"
          enabled
        >
          <Animated.View style={{ opacity: this.fadeAnimation }}>
            <Text style={styles.welcome}>{"Sign Up"}</Text>
          </Animated.View>
          <Animated.View style={{ opacity: this.fadeAnimation2 }}>
            <Text style={styles.info}>
              {"It's gonna take less than a minute"}
            </Text>
          </Animated.View>
          <Animated.View style={[styles.form, { opacity: this.boxes }]}>
            <TextInput
              label="Username"
              value={this.user}
              onChangeText={text => (this.user = text)}
              mode={"outlined"}
              secureTextEntry={false}
              returnKeyType={"next"}
              onSubmitEditing={() => {
                this.refs.secondTextInput.focus();
              }}
              blurOnSubmit={false}
              style={styles.textField}
            />
            <TextInput
              label="Password"
              value={this.password}
              onChangeText={text => (this.password = text)}
              mode={"outlined"}
              secureTextEntry={true}
              returnKeyType={"next"}
              ref={"this.secondTextInput"}
              style={styles.textField}
              onSubmitEditing={() => {
                this.refs.thirdTextInput.focus();
              }}
            />
            <TextInput
              label="Repeat password"
              value={this.repeatPassword}
              onChangeText={text => (this.repeatPassword = text)}
              mode={"outlined"}
              secureTextEntry={true}
              error={this.passwordsMatching()}
              ref={"thirdTextInput"}
              onSubmitEditing={() => {
                this.registration();
              }}
              blurOnSubmit={false}
              style={styles.textField}
            />
            <HelperText type="error" visible={this.passwordsMatching()}>
              Passwords not matching!
            </HelperText>
          </Animated.View>
        </KeyboardAvoidingView>
      );
    else
      return (
        <View style={styles.container}>
          <Animated.View style={{ opacity: this.fadeAnimation }}>
            <Text style={styles.welcome}>{"All Done!"}</Text>
          </Animated.View>
          <Animated.View style={{ opacity: this.fadeAnimation2 }}>
            <Text style={styles.info}>{"You may now log in"}</Text>
          </Animated.View>
        </View>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },
  textField: {
    width: "75%"
  },
  form: {
    width: "100%",
    alignItems: "center"
  },
  welcome: {
    fontSize: 40,
    marginBottom: 2
  },
  info: {
    fontSize: 15,
    marginBottom: 50,
    color: "grey"
  }
});
