/* @flow @format */
import React, { Component } from "react";
import {
  StyleSheet,
  FlatList,
  KeyboardAvoidingView,
  View,
  Image,
  Text,
  Animated
} from "react-native";
import { observer, inject } from "mobx-react/native";
import { observable } from "mobx";
import { TextInput } from "react-native-paper";

import { NavigationScreenProps } from "react-navigation";
import type { Stores } from "../types/Stores";

type StoreProps = {|
  login: (username: string, password: string) => void
|};

type OwnProps = {| navigation: NavigationScreenProps |};
type State = {};
type Props = StoreProps & OwnProps;

const mapStoresToProps = (stores: Stores, props: Props): StoreProps => {
  return {
    login: stores.authStore.login
  };
};

@inject(mapStoresToProps)
@observer
export default class Login extends Component<Props, State> {
  @observable user: string = "";
  @observable password: string = "";

  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <Text style={styles.welcome}>{"Welcome to ShitNews"}</Text>
        <Text style={styles.info}>{"Login in to see the latest news"}</Text>
        <Image
          source={require("../../assets/assets/images/poo.png")}
          style={styles.poop}
          resizeMode="contain"
        />
        <TextInput
          label="Username"
          value={this.user}
          onChangeText={text => (this.user = text)}
          mode={"outlined"}
          secureTextEntry={false}
          returnKeyType={"next"}
          onSubmitEditing={() => {
            this.refs.secondTextInput.focus();
          }}
          blurOnSubmit={false}
          style={styles.textField}
        />
        <TextInput
          label="Password"
          value={this.password}
          onChangeText={text => (this.password = text)}
          mode={"outlined"}
          secureTextEntry={true}
          ref={"secondTextInput"}
          style={styles.textField}
          onSubmitEditing={() => {
            this.props.login(this.user, "pwd");
          }}
        />
      </KeyboardAvoidingView>
    );
  }

  static navigationOptions = {
    title: "ShitNews"
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },
  textField: {
    width: "75%"
  },
  poop: {
    width: "70%",
    flex: 0.3,
    marginBottom: 50
  },
  welcome: {
    fontSize: 40,
    marginBottom: 2
  },
  info: {
    fontSize: 15,
    marginBottom: 50,
    color: "grey"
  }
});
