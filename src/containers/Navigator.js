/* @flow @format */
import React from "react";
import { Platform } from "react-native";
import TabBarIcon from "../components/TabBarIcon";
import {
  createStackNavigator,
  createBottomTabNavigator,
  createAppContainer
} from "react-navigation";

import Home from "./Home";
import Bookmarks from "./Bookmarks";
import PostView from "./PostView";
import Test from "./Test";
import Gps from "./Gps";
import Beacons from "./Beacons";
import Audio from "./Audio";
import Place from "./Place";

const topBarStyle = {
  headerStyle: {
    backgroundColor: "#f4511e"
  },
  headerTintColor: "#fff",
  headerTitleStyle: {
    fontWeight: "bold"
  }
};

const HomeStack = createStackNavigator(
  {
    Home,
    Post: PostView
  },
  {
    initialRouteName: "Home",
    defaultNavigationOptions: topBarStyle
  }
);

HomeStack.navigationOptions = {
  tabBarLabel: "Home",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === "ios"
          ? `ios-information-circle${focused ? "" : "-outline"}`
          : "md-information-circle"
      }
    />
  )
};

const BookmarksStack = createStackNavigator(
  {
    Bookmarks,
    Post: PostView
  },
  {
    initialRouteName: "Bookmarks",
    defaultNavigationOptions: topBarStyle
  }
);

BookmarksStack.navigationOptions = {
  tabBarLabel: "Bookmarks",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? "ios-heart" : "md-heart"}
    />
  )
};

const TestStack = createStackNavigator(
  {
    Test,
    Page: PostView
  },
  {
    initialRouteName: "Test",
    defaultNavigationOptions: topBarStyle
  }
);

TestStack.navigationOptions = {
  tabBarLabel: "QRCode",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? "ios-qr-scanner" : "md-qr-scanner"}
    />
  )
};

const GPSStack = createStackNavigator(
  {
    Gps,
    Place
  },
  {
    initialRouteName: "Gps",
    defaultNavigationOptions: topBarStyle
  }
);

GPSStack.navigationOptions = {
  tabBarLabel: "Gps",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? "ios-locate" : "md-locate"}
    />
  )
};

const BeaconsStack = createStackNavigator(
  {
    Beacons
  },
  {
    initialRouteName: "Beacons",
    defaultNavigationOptions: topBarStyle
  }
);

BeaconsStack.navigationOptions = {
  tabBarLabel: "Beacons",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? "ios-nuclear" : "md-nuclear"}
    />
  )
};

const AudioStack = createStackNavigator(
  {
    Audio
  },
  {
    initialRouteName: "Audio",
    defaultNavigationOptions: topBarStyle
  }
);

AudioStack.navigationOptions = {
  tabBarLabel: "Audio",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? "ios-musical-note" : "md-musical-note"}
    />
  )
};

const MainNavigator = createBottomTabNavigator(
  {
    HomeStack,
    BookmarksStack,
    TestStack,
    GPSStack,
    BeaconsStack,
    AudioStack
  },
  {
    initialRouteName: "AudioStack",
    defaultNavigationOptions: topBarStyle
  }
);

export default createAppContainer(MainNavigator);
