/* @flow @format */
import React from "react";
import { Platform } from "react-native";
import TabBarIcon from "../components/TabBarIcon";
import Login from "./Login";
import Registration from "./Registration";
import {
  createStackNavigator,
  createBottomTabNavigator,
  createAppContainer
} from "react-navigation";

const topBarStyle = {
  headerStyle: {
    backgroundColor: "#f4511e"
  },
  headerTintColor: "#fff",
  headerTitleStyle: {
    fontWeight: "bold"
  }
};

const LoginStack = createStackNavigator(
  {
    Login
  },
  {
    initialRouteName: "Login",
    defaultNavigationOptions: topBarStyle
  }
);

LoginStack.navigationOptions = {
  tabBarLabel: "Login",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? `ios-log-in` : "md-log-in"}
    />
  )
};

const RegisterStack = createStackNavigator(
  {
    Registration
  },
  {
    initialRouteName: "Registration",
    defaultNavigationOptions: topBarStyle
  }
);

RegisterStack.navigationOptions = {
  tabBarLabel: "Registration",
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === "ios" ? "ios-person" : "md-person"}
    />
  )
};

const Navigator = createBottomTabNavigator(
  {
    LoginStack,
    RegisterStack
  },
  {
    initialRouteName: "LoginStack",
    defaultNavigationOptions: topBarStyle
  }
);

export default createAppContainer(Navigator);
