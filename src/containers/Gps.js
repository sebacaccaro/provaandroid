/* @flow @format */
import React, { Component, Element } from "react";
import { observer } from "mobx-react/native";
import { observable, computed } from "mobx";
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  View,
  Button,
  FlatList,
  Image,
  SectionList,
  Platform
} from "react-native";
import { NavigationScreenProps } from "react-navigation";
import { PermissionsAndroid } from "react-native";
import { getDistance } from "geolib";
import { markers } from "../constants/GpsMarkers";
import type { Coord, GpsCardInfo } from "../types/Gps";
import LocationCard from "../components/LocationCard";
import Geolocation from "@react-native-community/geolocation";
import DeviceInfo from "react-native-device-info";
import w from "../constants/Layout";

type StoreProps = {};
type OwnProps = {| navigation: NavigationScreenProps |};
type State = {};
type Props = StoreProps & OwnProps;

const requestGpsPermissionAndroid = async (): Promise<boolean> => {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: "Gps Permission",
        message: "Shitnews would like to get your position to steal your data!",
        buttonNeutral: "Ask Me Later",
        buttonNegative: "Cancel",
        buttonPositive: "OK"
      }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    console.warn(err);
    return false;
  }
};

const requestGpsPermission = async (): Promise<boolean> => {
  let permission = true;
  if (Platform.OS === "ios") permission = await requestGpsPermissionIos();
  else permission = await requestGpsPermissionAndroid();
  return permission;
};

const requestGpsPermissionIos = async (): Promise<boolean> => {
  await Geolocation.requestAuthorization();
  return true;
};

@observer
export default class Gps extends Component<Props, State> {
  @observable coords: Coord = {};

  @computed
  get SortedLocations(): Array<Coord> {
    const toSort = markers.map(marker => ({
      name: marker.name,
      distance: getDistance(this.coords, marker.coords),
      notifyDistance: marker.notififyDistance,
      image: marker.image
    }));
    return toSort.sort((c1, c2) => c1.distance - c2.distance);
  }

  @computed
  get locationsInSections(): Array<Coord> {
    return [
      {
        title: "Near you",
        data: this.SortedLocations.filter(location => location.distance < 10000)
      },
      {
        title: "Far away",
        data: this.SortedLocations.filter(
          location => location.distance >= 10000
        )
      }
    ];
  }

  componentDidMount() {
    requestGpsPermission().then(permissionGranted => {
      console.log(permissionGranted);
      if (permissionGranted) {
        Geolocation.watchPosition(
          position => this.updatePosition(position),
          () => {},
          {
            enableHighAccuracy: false,
            maximumAge: 2000,
            distanceFilter: 5
          }
        );
      } else {
        console.log("Fuck!");
      }
    });
  }

  updatePosition = (position: { coords: Coord }): void => {
    this.coords = position.coords;
  };

  renderLocation = (location: GpsCardInfo): Element<typeof LocationCard> => {
    return <LocationCard marker={location} />;
  };

  extractKey = (location: GpsCardInfo): string => {
    return location.name + "loc";
  };

  renderSectionHeader = section => {
    if (Object.keys(section.section.data).length === 0) return null;
    return (
      <View
        style={{
          flexDirection: "row",
          marginTop: 15,
          marginBottom: 15,
          width: w.window.width
        }}
      >
        <View
          style={{
            backgroundColor: "grey",
            height: 1,
            flex: 1,
            alignSelf: "center",
            marginLeft: 10
          }}
        />
        <Text
          style={{ alignSelf: "center", paddingHorizontal: 5, fontSize: 15 }}
        >
          {section.section.title}
        </Text>
        <View
          style={{
            backgroundColor: "grey",
            height: 1,
            flex: 1,
            alignSelf: "center",
            marginRight: 10
          }}
        />
      </View>
    );
  };

  render() {
    if (Object.keys(this.coords).length !== 0)
      return (
        <View style={styles.container}>
          <SectionList
            sections={this.locationsInSections}
            renderItem={this.renderLocation}
            renderSectionHeader={this.renderSectionHeader}
            keyExtractor={this.extractKey}
            stickySectionHeadersEnabled={false}
            contentContainerStyle={{
              flexWrap: "wrap",
              flexDirection: "row"
            }}
          />
        </View>
      );
    else
      return (
        <View style={styles.container2}>
          <Text style={styles.textBold}>{"Loading location"}</Text>
          <Image
            style={styles.img}
            source={require("../../assets/assets/images/tenor.gif")}
          />
        </View>
      );
  }

  static navigationOptions = {
    title: "Location"
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    flexWrap: "wrap",
    flexDirection: "row",
    alignItems: "flex-start"
  },
  container2: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "space-evenly"
  },
  section: {
    color: "black",
    fontSize: 20,
    width: "100%"
  },
  img: {
    borderRadius: 5
  },
  textBold: {
    fontSize: 40
  },
  buttonText: {
    fontSize: 21,
    color: "rgb(0,122,255)"
  },
  buttonTouchable: {
    padding: 16
  },
  title: {
    fontSize: 25
  },
  measure: {
    fontSize: 15
  },
  coord: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});
