/* @flow @format */
import React, { Component } from "react";
import { observer } from "mobx-react/native";
import { observable } from "mobx";
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  View
} from "react-native";
import { NavigationScreenProps } from "react-navigation";
import { PermissionsAndroid } from "react-native";
import Beacons from "react-native-beacons-manager";
import beaconList from "../constants/BeaconsConfig";
import { DeviceEventEmitter } from "react-native";

const closest = 0.004;
const furthest = 2;

const capitalize = s => {
  if (typeof s !== "string") return "";
  return s.charAt(0).toUpperCase() + s.slice(1);
};

type StoreProps = {};
type OwnProps = {| navigation: NavigationScreenProps |};
type State = {};
type Props = StoreProps & OwnProps;

@observer
export default class Beacon extends Component<Props, State> {
  @observable peppinoDistance: number = 0;
  @observable peppinoProximity: string = "Error 404 \n Peppino not found";

  componentDidMount() {
    this.starAll();
    Beacons.requestWhenInUseAuthorization();
  }

  starAll = async () => {
    //Beacons.detectIBeacons();
    const region2 = {
      identifier: "peppini",
      uuid: "f7826da6-4fa2-4e98-8024-bc5b71e0893e"
    };

    const region1 = {
      identifier: "estimote",
      uuid: "B9407F30-F5F8-466E-AFF9-25556B57FE6D"
    };

    const peppino = {
      minor: 1199,
      uniqueId: "5Wkz",
      major: 35132
    };
    // Start detecting all iBeacons in the nearby
    try {
      await Beacons.startRangingBeaconsInRegion(region2);
      console.log(`Beacons ranging started succesfully!`);
    } catch (err) {
      console.log(`Beacons ranging not started, error: ${error}`);
    }

    DeviceEventEmitter.addListener("beaconsDidRange", data => {
      this.updateDistance(
        data.beacons.filter(
          beacon =>
            Object.keys(beaconList).indexOf(
              `${beacon.major}:${beacon.minor}`
            ) >= 0
        )
      );
    });

    DeviceEventEmitter.addListener("regionDidExit", data => {
      console.log("out", data);
    });
  };

  @observable lastUpdate = new Date().getTime() / 1000 + 3600;
  updateDistance = found => {
    console.log("in", found);
    if (found.length > 0) {
      this.peppinoDistance = found[0].distance;
      this.peppinoProximity = found[0].proximity;
      this.lastUpdate = new Date().getTime() / 1000;
    } else {
      if (new Date().getTime() / 1000 - this.lastUpdate > 10) {
        this.peppinoDistance = 0;
        this.peppinoProximity = "Error 404 \nPeppino non found";
      }
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>{capitalize(this.peppinoProximity)}</Text>
        <Text style={styles.measure}>{this.peppinoDistance}</Text>
      </View>
    );
  }

  static navigationOptions = {
    title: "Beacons"
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  textBold: {
    fontWeight: "500",
    color: "#000"
  },
  buttonText: {
    fontSize: 21,
    color: "rgb(0,122,255)"
  },
  buttonTouchable: {
    padding: 16
  },
  title: {
    fontSize: 40
  },
  measure: {
    fontSize: 25
  },
  coord: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});
